from django.test import TestCase
from django.urls import resolve
from .views import index

# Create your tests here.
class Test(TestCase):
    def test_url_is_exist(self):
        response=self.client.get('')
        self.assertEqual(response.status_code,200)
    
    def test_using_landing_page_template(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

